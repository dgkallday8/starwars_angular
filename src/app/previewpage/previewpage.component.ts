import { Component, OnInit } from '@angular/core'
import { HttpService } from '../http.service'
import { ActivatedRoute, Router } from '@angular/router'

export interface IParameter {
  id: string
  value: string
}

export interface ICharacter {
  id: string
  name: string
  imageURL: string,
  description: string,
  gender: string | IParameter
  race: string | IParameter
  side: string | IParameter
  backgroundColor: string
  nameColor: string
  parametersColor: string
}


@Component({
  selector: 'app-previewpage',
  templateUrl: './previewpage.component.html',
  styleUrls: ['./previewpage.component.scss'],
  providers: []
})
export class PreviewPageComponent implements OnInit {

  constructor (public httpService: HttpService, public activeRoute: ActivatedRoute, public router: Router) {}

  ngOnInit(): void {

    if (!Object.keys(this.activeRoute.snapshot.queryParams).length) {
      this.httpService.getData()
    } else {
      this.httpService.searchValue = this.activeRoute.snapshot.queryParams['values']
      this.httpService.getData()
    }
  }

}
