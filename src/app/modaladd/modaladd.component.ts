import { Component, ElementRef, OnInit, ViewChild } from '@angular/core'
import { HttpService } from '../http.service'
import { FormControl, FormGroup, NgForm, Validators } from '@angular/forms'
import { genderHandler, raceHandler, sideHandler } from '../utils'
import { ICharacter } from '../previewpage/previewpage.component'
import { Router } from '@angular/router'

@Component({
  selector: 'app-modaladd',
  templateUrl: './modaladd.component.html',
  styleUrls: ['./modaladd.component.scss'],
  providers: []
})
export class ModalAddComponent implements OnInit {

//@ts-ignore
  @ViewChild('imgInput') imgInput: ElementRef
//@ts-ignore
  theForm: FormGroup

  descriptionMaxLength = 100
  image = ''

  testValue = {
    "name": "Testestest",
    "description": "Beautiful forest. Whether you're looking for a little trip inspiration or need a virtual escape, these shots of 2,000-year-old redwoods, thick green bamboo.",
    "imageURL": "https://www.elopak.com/wp-content/uploads/2019/12/shutterstock_516051958-1024x683.jpg",
    "nameColor": "#00ffc1",
    "backgroundColor": "#fd3749",
    "parametersColor": "#074ffd",
    "tag1": "Nature",
    "tag2": "Freedom",
    "tag3": "Trees",
    "gender": {
      "id": "6de244eb-591b-4fbe-a023-3f33bd45db21",
      "value": "Неизвестен"
    },
    "race": {
      "id": "5b2bcf2d-6560-4320-9603-70ad5785e8cf",
      "value": "Неизвестен"
    },
    "side": {
      "id": "aa26c7e3-02a1-4697-8b27-fb24ae1739dc",
      "value": "Неизвестен"
    }
  }

  constructor(public httpService: HttpService, private router: Router) { }

  myForm = NgForm

  ngOnInit(): void {
    this.theForm = new FormGroup({
      name: new FormControl('', [Validators.required]),
      description: new FormControl('', [Validators.maxLength(this.descriptionMaxLength)]),
      gender: new FormControl('Gender'),
      race: new FormControl('Race'),
      side: new FormControl('Side'),
      nameColor: new FormControl('#eeeeee'),
      backgroundColor: new FormControl('#3d3d3d'),
      parametersColor: new FormControl('#cccccc'),
      imageURL: new FormControl('', [Validators.required]),
      tag1: new FormControl(''),
      tag2: new FormControl(''),
      tag3: new FormControl(''),

    })
  }

  closeAddModal(e: Event) {
    const target = e.target as HTMLElement
    if (target.classList.contains('add-modal')) {
      this.httpService.toggle()
    }
  }


  submit() {
    if (this.theForm.valid) {
      // console.log('form', this.theForm)
      const formData = {...this.theForm.value}

      // console.log(formData)
      const newData = this.transformData(formData) as ICharacter

      this.httpService.postData(newData)
      this.router.navigate(['/preview'])
    }

  }


  loadFile(event: Event) {
    const target = event.target as HTMLInputElement
    const files = target['files'] as FileList
    const file: File = files[0]
    const reader: FileReader = new FileReader()
    reader.readAsDataURL(file)
    reader.onload = () => {
      const img = reader.result as string
      this.theForm.get('imageURL')?.setValue(img)
      this.image = img
    }

  }

  clearImage() {
    this.theForm.get('imageURL')?.setValue('')
    this.image = ''
  }

  transformData(formObj: any) {
    return {
      ...formObj,
      gender: genderHandler(this.theForm.get('gender')?.value),
      side: sideHandler(this.theForm.get('side')?.value),
      race: raceHandler(this.theForm.get('race')?.value)
    }
  }


}
