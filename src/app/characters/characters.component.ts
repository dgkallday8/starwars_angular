import { Component, OnInit } from '@angular/core'
import { HttpService } from '../http.service'

@Component({
  selector: 'app-characters',
  templateUrl: './characters.component.html',
  styleUrls: ['./characters.component.scss']
})
export class CharactersComponent implements OnInit {


  constructor(public httpService: HttpService) { }

  ngOnInit(): void {}

}
