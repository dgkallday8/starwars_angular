import { Component, Input, OnInit } from '@angular/core'
import { HttpService } from '../http.service'

@Component({
  selector: 'app-pagination',
  templateUrl: './pagination.component.html',
  styleUrls: ['./pagination.component.scss'],
  providers: []
})
export class PaginationComponent implements OnInit {

  constructor(public httpService: HttpService) {}

  ngOnInit(): void {}

}
