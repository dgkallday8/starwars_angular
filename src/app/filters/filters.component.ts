import { Component, OnInit } from '@angular/core'
import { HttpService } from '../http.service'
import { Router } from '@angular/router'


@Component({
  selector: 'app-filters',
  templateUrl: './filters.component.html',
  styleUrls: ['./filters.component.scss']
})
export class FiltersComponent implements OnInit {

  constructor(public httpService: HttpService, public router: Router) { }

  ngOnInit(): void {}

}
