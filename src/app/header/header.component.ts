import { Component, OnInit } from '@angular/core'
import { HttpService } from '../http.service'

export interface INavLink {
  text: string
  path: string
}

@Component({
  selector: 'app-header',
  templateUrl: './header.component.html',
  styleUrls: ['./header.component.scss']
})

export class HeaderComponent implements OnInit {

  constructor(private httpService: HttpService) { }

  imgUrl = '../../assets/logo.svg'

  navigationLinks: INavLink[] = [
    {text: 'Home', path: '/'},
    {text: 'Preview', path: '/preview'}
  ]

  ngOnInit(): void {}

  clearAllFilters() {
    this.httpService.searchValue = ''
    this.httpService.getData()
  }

}
