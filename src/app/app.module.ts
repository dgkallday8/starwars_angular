import { NgModule } from '@angular/core'
import { BrowserModule } from '@angular/platform-browser'
import { FormsModule, ReactiveFormsModule } from '@angular/forms'

import { AppComponent } from './app.component'
import { HeaderComponent } from './header/header.component'
import { StartPageComponent } from './startpage/startpage.component';
import { ButtonComponent } from './button/button.component'
import { RouterModule, Routes } from '@angular/router';
import { PreviewPageComponent } from './previewpage/previewpage.component';
import { PaginationComponent } from './pagination/pagination.component';
import { CharactersComponent } from './characters/characters.component';
import { FiltersComponent } from './filters/filters.component'
import { HttpClientModule } from '@angular/common/http';
import { CardComponent } from './card/card.component';
import { ModalCardComponent } from './modalcard/modalcard.component';
import { NotfoundComponent } from './notfound/notfound.component';
import { ModalAddComponent } from './modaladd/modaladd.component';
import { CheckboxComponent } from './checkbox/checkbox.component'

const appRoutes: Routes = [
  { path: '', component: StartPageComponent },
  { path: 'preview', component: PreviewPageComponent },
  { path: 'preview/add', component: ModalAddComponent },
  { path: 'preview/:id', component: ModalCardComponent },
  { path: 'notfound', component: NotfoundComponent },
  { path: '**', redirectTo: '/notfound' }
]

@NgModule({
  declarations: [
    AppComponent,
    HeaderComponent,
    StartPageComponent,
    ButtonComponent,
    PreviewPageComponent,
    PaginationComponent,
    CharactersComponent,
    FiltersComponent,
    CardComponent,
    ModalCardComponent,
    NotfoundComponent,
    ModalAddComponent,
    CheckboxComponent
  ],
  imports: [
    BrowserModule,
    FormsModule,
    ReactiveFormsModule,
    RouterModule.forRoot(appRoutes),
    HttpClientModule
  ],
  providers: [],
  bootstrap: [AppComponent]
})
export class AppModule { }
