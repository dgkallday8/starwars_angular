import { Component, Input, OnInit } from '@angular/core'
import { Router } from '@angular/router'
import { HttpService } from '../http.service'

@Component({
  selector: 'app-startpage',
  templateUrl: './startpage.component.html',
  styleUrls: ['./startpage.component.scss']
})
export class StartPageComponent implements OnInit {

  buttonText: string = 'Start'

  constructor(public router: Router, public httpService: HttpService) { }

  ngOnInit(): void {
    this.httpService.openedWindowAdd = false
  }

  goToPreviewPage() {
    this.router.navigate(['/preview']).then()
  }

}
