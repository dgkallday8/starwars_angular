import { Component, ElementRef, Input, OnInit, ViewChild } from '@angular/core'
import { HttpService, IFilterValues } from '../http.service'

@Component({
  selector: 'app-checkbox',
  templateUrl: './checkbox.component.html',
  styleUrls: ['./checkbox.component.scss']
})

export class CheckboxComponent implements OnInit {


  @ViewChild('imgRef') imgRef: ElementRef | undefined
  @ViewChild('listRef') listRef: ElementRef | undefined

  @Input() title = ''
  @Input() values: string[] = []

  hide = true
  rotate = false

  constructor(public httpService: HttpService) {}

  ngOnInit(): void {}

  logEvent(event: Event) {
    const target = event.target as HTMLInputElement
    const value = target.value

    // @ts-ignore
    let targetArr: string[] = this.httpService.filterValues[this.title.toLowerCase()]

    if (targetArr.includes(value)) {
      targetArr = targetArr.filter((v: string) => v !== value)
    } else {
      targetArr.push(value)
    }

    this.httpService.filterValues = {
      ...this.httpService.filterValues,
      [this.title.toLowerCase()]: targetArr
    }
    console.log('FILTER ValUES: ', this.httpService.filterValues)
    this.httpService.getData()
  }

  rotateFn() {
    this.hide = !this.hide
    this.rotate = !this.rotate
  }

  checkFilterValue(title: string, value: string): boolean {
    //@ts-ignore
    return !!this.httpService.filterValues[title].includes(value)
  }
}
