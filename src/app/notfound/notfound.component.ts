import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router'

@Component({
  selector: 'app-notfound',
  templateUrl: './notfound.component.html',
  styleUrls: ['./notfound.component.scss']
})
export class NotfoundComponent implements OnInit {

  buttonText: string = 'Return'

  constructor(public router: Router) { }

  ngOnInit(): void {}

  goToStartPage() {
    this.router.navigate(['/']).then()
  }

}
