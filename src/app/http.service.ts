import { Injectable, Input } from '@angular/core'
import { HttpClient, HttpHeaders } from '@angular/common/http'
import { ICharacter, IParameter } from './previewpage/previewpage.component'
import { getArrayLength } from './utils'
import { ActivatedRoute, Router } from '@angular/router'
import { Observable } from 'rxjs'
import { filterValuesObject } from './values'


export interface IGetCharacter {
  id: string
  name: string
  imageURL: string,
  description: string,
  gender: IParameter
  race: IParameter
  side: IParameter
  backgroundColor: string
  nameColor: string
  parametersColor: string
}

export interface IFilterValues {
  gender: string[],
  race: string[],
  side: string[]
}

@Injectable({
  providedIn: 'root'
})

export class HttpService {

  activeIndex = 0
  numbersOfViewCards = 3
  length = 0

  @Input() searchValue: string = ''
  @Input() filterValues: IFilterValues = {
    gender: [],
    race: [],
    side: []
    // gender: ['Неизвестен', 'Мужчина'],
    // race: ['Дроид', 'Вуки'],
    // side: ['Светлая']
  }

  openedWindowAdd: boolean = false


  charactersData: ICharacter[] = []
  dots: string[] = []

  mainPartUrl: string = 'http://localhost:5000/api/STAR_WARS/character?'

  currentPerson: IGetCharacter | undefined

  constructor(private http: HttpClient, private router: Router, public activatedRoute: ActivatedRoute){ }


  getData(): void{
    console.log(this.activatedRoute.snapshot.queryParams)
    const url = `
      ${this.mainPartUrl}values=${this.searchValue}&page=${this.activeIndex}&size=${this.numbersOfViewCards}${this.convertFilters(this.filterValues)}
    `

    this.http.get(url).subscribe((data: any) => {
      this.charactersData = data.content
      this.dots = getArrayLength(data.totalElements)
      this.length = this.dots.length
    })
    if (this.searchValue.length) {
      this.router.navigate(['preview'], {
        queryParams: {
          values: this.searchValue,
        }
      }).then()
    } else {
      this.router.navigate(['preview'])
    }
  }

  increaseActiveIndex() {
    if (this.activeIndex === this.length - 1) {
      this.activeIndex = this.length - 1
    } else {
      this.activeIndex++
      this.getData()
    }
  }
  decreaseActiveIndex(): void {
    if (this.activeIndex === 0) {
      this.activeIndex = 0
    } else {
      this.activeIndex--
      this.getData()
    }
  }

  setActiveIndex(index: number) {
    if (this.activeIndex === index) {
      return
    }
    this.activeIndex = index
    this.getData()
    this.convertFilters(this.filterValues)
  }

  getPersonInfo(id: string) {
    const url = `http://localhost:5000/api/STAR_WARS/character/${id}`
    this.http.get(url).subscribe((data: any) => {
      this.currentPerson = data
    }, error => {
      this.router.navigate(['/notfound'])
    })
  }

  toggle() {
    this.openedWindowAdd = !this.openedWindowAdd
    if (this.router.url === '/preview') {
      this.router.navigate(['preview', 'add'])
    } else {
      this.router.navigate(['preview'])
    }
    this.getData()
  }

  postData(data: ICharacter) {
    const headers = new HttpHeaders().set('Content-Type', 'application/json;charset=utf-8')

    return this.http.post('http://localhost:5000/api/STAR_WARS/character', data, {
      headers
    })
      .subscribe()
  }

  convertFilters(object: IFilterValues): string {

    const arr = Object.entries(object)
    let str = ''
    for (let filterParam of arr) {
      const f = filterParam[0]
      const values = filterParam[1]
      for (let value of values) {
        // @ts-ignore
        str += `&${f}=${filterValuesObject[f][value]}`
      }
    }
    return str
  }

}
