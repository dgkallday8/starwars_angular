import { Component, Input, OnInit } from '@angular/core'
import { ICharacter } from '../previewpage/previewpage.component'
import { ActivatedRoute } from '@angular/router'

@Component({
  selector: 'app-card',
  templateUrl: './card.component.html',
  styleUrls: ['./card.component.scss']
})
export class CardComponent implements OnInit {

  @Input() dataPerson:ICharacter | undefined

  constructor(public router: ActivatedRoute) {}

  ngOnInit(): void {}


}
