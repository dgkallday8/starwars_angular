import { IParameter } from './previewpage/previewpage.component'

export function getArrayLength(num: number): string[] {
  const length = Math.ceil(num / 3) > 5 ? 5 : Math.ceil(num / 3)
  return Array(length).fill('')
}

export function genderHandler(paramName: string): IParameter {
  switch (paramName) {
    case 'male':
      return {
        id: '0621a5a3-7235-419d-87e7-2f5b6d9d87c8',
        value: 'Мужчина'
      }
    case 'female':
      return {
        id: '4be23167-9bea-4167-9f02-4d921124015c',
        value: 'Женщина'
      }
    default:
      return  {
        id: '6de244eb-591b-4fbe-a023-3f33bd45db21',
        value: 'Неизвестен'
      }
  }
}

export function raceHandler(paramName: string): IParameter {
  switch (paramName) {
    case 'man':
      return {
        id: '6676bbc3-ab05-401b-9406-66bfd52ca0c0',
        value: 'Человек'
      }
    case 'droid':
      return {
        id: '93905331-66ff-4703-bf9f-96f5a7d27a00',
        value: 'Дроид'
      }
    case 'wookiee':
      return {
        id: '1fd77e91-02c1-4ebb-9a90-971b4f81a36c',
        value: 'Вуки'
      }
    default:
      return  {
        id: '5b2bcf2d-6560-4320-9603-70ad5785e8cf',
        value: 'Неизвестен'
      }
  }
}

export function sideHandler(paramName: string): IParameter {
  switch (paramName) {
    case 'light':
      return {
        id: 'bd4c2123-b3a1-4a34-a5d7-f7777fe7f758',
        value: 'Светлая'
      }
    case 'dark':
      return {
        id: 'e1a5f26f-c2f8-4d02-8c71-a9bf82cfb1f5',
        value: 'Темная'
      }
    default:
      return {
        id: 'aa26c7e3-02a1-4697-8b27-fb24ae1739dc',
        value: 'Неизвестен'
      }
  }
}
