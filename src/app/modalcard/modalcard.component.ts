import { Component, OnInit } from '@angular/core'
import { Location } from '@angular/common';
import { ActivatedRoute, Router } from '@angular/router'
import { HttpService } from '../http.service'

@Component({
  selector: 'app-modalcard',
  templateUrl: './modalcard.component.html',
  styleUrls: ['./modalcard.component.scss']
})
export class ModalCardComponent implements OnInit {

  constructor(
    private location: Location,
    private routerActive: ActivatedRoute,
    private router: Router,
    public httpService: HttpService) { }

  idPerson: string = ''

  ngOnInit(): void {
    const urlParams = this.routerActive.snapshot.params
    if (Object.keys(urlParams).length) {
      this.idPerson = this.routerActive.snapshot.params['id']
      this.httpService.getPersonInfo(this.idPerson)
    }
  }

  modalHandler($event: MouseEvent): void {
    const target = $event.target as HTMLElement
    if (target.classList.contains('card__modal')) {
      this.location.back()
    }
  }

  closeIcon() {
    this.router.navigate(['/preview']).then()
  }

}
